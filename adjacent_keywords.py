#Given a list of keywords and a list of search words,
#return a list of indices that indicate the beginning
#of a sequence of all adjacent keywords.



def needle_indices(haystack, needle):
    for value in haystack:
        if value in needle:
            yield needle.index(value)
        else:
            yield None

def keyword_search(haystack, needle):
    num_keywords = len(needle)
    encountered_indices = []
    for idx, kw_idx in enumerate(needle_indices(haystack, needle)):
        if kw_idx is None:
            encountered_indices = []
            continue
        if kw_idx in encountered_indices:
            begin = encountered_indices.index(kw_idx)
            encountered_indices = encountered_indices[begin + 1:]
        encountered_indices.append(kw_idx)
        if len(encountered_indices) == num_keywords:
            yield idx - (num_keywords - 1)
            encountered_indices = encountered_indices[1:]

#print("\n".join(
#    [str(x) for x in needle_indices(search_list, keywords)]
#))

search_list = ['hello', 'hi', 'hi', 'greetings', 'hi', 'greetings', 'hey', 'hi']
keywords = ['hi', 'hey', 'greetings']
output = keyword_search(search_list, keywords)
assert list(output) == [4, 5]

search_list = ['peter', 'piper', 'picked', 'a', 'peck', 'of', 'pickled', 'peppers', 'a',
               'peck', 'of', 'pickled', 'peppers', 'peter', 'piper', 'picked', 'if',
               'peter', 'piper', 'picked', 'a', 'peck', 'of', 'pickled', 'peppers',
               'wheres', 'the', 'peck', 'of', 'pickled', 'peppers', 'peter', 'piper', 'picked']
keywords = ['a', 'peter', 'picked', 'piper']
output = keyword_search(search_list, keywords)
assert list(output) == [0, 17]


search_list = ['i', 'saw', 'susie', 'sitting', 'in', 'a', 'shoe', 'shine', 'shop', 'where', 'she',
               'sits', 'she', 'shines', 'and', 'where', 'she', 'shines', 'she', 'sits']
keywords = ['she', 'sits', 'shines']
output = keyword_search(search_list, keywords)
assert list(output) == [11, 17]
